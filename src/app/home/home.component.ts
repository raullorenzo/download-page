import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fadeAnimation, leaveAnimation, leaveRightAnimation, leaveLeftAnimation } from './../animations/animations';
import { DeviceDetectorService } from 'ngx-device-detector';
import { DeviceInfo } from '../interfaces/device-info';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    fadeAnimation, leaveAnimation,
    leaveRightAnimation, leaveLeftAnimation
  ]
})
export class HomeComponent implements OnInit {

  deviceInfo: DeviceInfo = null;
  isMobile: boolean;
  isTablet: boolean;
  isDesktop: boolean;
  next: boolean;
  prev: boolean;
  pics: string[];
  index = 0;

  constructor(private deviceService: DeviceDetectorService) {
    this.next = true;
    this.prev = true;
    this.pics = [
      'iphone-wateca-app-cli-ios',
      'iphone-wateca-app-cli',
      'iphone-wateca'
    ];
  }

  ngOnInit(): void {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = this.deviceService.isMobile();
    this.isTablet = this.deviceService.isTablet();
    this.isDesktop = this.deviceService.isDesktop();

    if (this.deviceInfo.os_version === 'mac-os-x-13') {
      this.isDesktop = true;
      this.isMobile = false;
      this.isTablet = false;
    }

    console.log('device info:', this.deviceInfo); // returns User Agent info.
    console.log('isMobile:', this.isMobile);  // returns if the device is a mobile device (android / iPhone / windows-phone etc)
    console.log('isTablet:', this.isTablet);  // returns if the device us a tablet (iPad etc)
    console.log('isDesktop:', this.isDesktop); // returns if the app is running on a Desktop browser.
  }

  public downloadApk(): void { window.alert('download link APK'); }

  nextPic(): void { if (this.index < 2) { this.index++; } }

  previousPic(): void { if (this.index > 0) { this.index--; } }

}
