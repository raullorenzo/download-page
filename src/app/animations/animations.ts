import {
  trigger,
  animate,
  transition,
  style,
  state
} from '@angular/animations';

export const fadeAnimation = trigger('fadeInOut', [
  state('void', style({
    opacity: 0
  })),
  transition('void <=> *', animate(1500)),
]);

export const leaveAnimation = trigger('EnterLeave', [
  state('flyIn', style({
    transform: 'translateY(0)'
  })),
  transition(':enter', [
    style({
      transform: 'translateY(-100%)'
    }),
    animate('0.5s 200ms ease-in')
  ])
]);

export const leaveRightAnimation = trigger('EnterRightLeave', [
  state('flyIn', style({
    transform: 'translateX(0)'
  })),
  transition(':enter', [
    style({
      transform: 'translateX(-100%)'
    }),
    animate('0.5s 200ms ease-in')
  ])
]);

export const leaveLeftAnimation = trigger('EnterLeftLeave', [
  state('flyIn', style({
    transform: 'translateX(0)'
  })),
  transition(':enter', [
    style({
      transform: 'translateX(100%)'
    }),
    animate('0.5s 200ms ease-in')
  ])
]);
