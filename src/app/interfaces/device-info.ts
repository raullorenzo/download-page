export interface DeviceInfo {
  browser: string;
  browser_version: string;
  device: string;
  os: string;
  os_version: string;
  userAgent: string;
}
